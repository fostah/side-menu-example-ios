//
//  AppRootViewController.swift
//  SideMenuExample
//
//  Created by Michael Foster on 3/27/15.
//  Copyright (c) 2015 fostah.com. All rights reserved.
//

import UIKit

class AppRootViewController: UIViewController, MenuViewControllerDelegate {

    @IBOutlet weak var contentLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentRightConstraint: NSLayoutConstraint!
    
    private var menuViewController: MenuViewController! {
        didSet {
            menuViewController.delegate = self
        }
    }
    
    private var contentViewController: ContentViewController! {
        didSet {
            contentViewController.menuButton.target = self
            contentViewController.menuButton.action = "menuButtonPressed:"
        }
    }
    
    private var menuPresented = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MenuEmbedSegue" {
            if let vc = segue.destinationViewController as? MenuViewController {
                menuViewController = vc
            }
        } else if segue.identifier == "ContentEmbedSegue" {
            if let navc = segue.destinationViewController as? UINavigationController {
                if let vc = navc.viewControllers.last as? ContentViewController {
                    contentViewController = vc
                }
            }
        }
    }
    
    func menuButtonPressed(sender: UIBarButtonItem) {
        if menuPresented {
            contentLeftConstraint.constant = 0
            contentRightConstraint.constant = 0;
        } else {
            let constant = view.frame.width - 100
            contentLeftConstraint.constant = constant
            contentRightConstraint.constant = -constant
        }
        menuPresented ^= true
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func menuViewController(viewController: MenuViewController, didSelectMenuItemAtIndexPath indexPath: NSIndexPath) {
        contentViewController.contentContainerViewController.selectedIndex = indexPath.row
    }
}
