//
//  ContentViewController.swift
//  SideMenuExample
//
//  Created by Michael Foster on 3/27/15.
//  Copyright (c) 2015 fostah.com. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!

    var contentContainerViewController: UITabBarController! {
        didSet {
            contentContainerViewController.tabBar.translucent = true
            contentContainerViewController.tabBar.hidden = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TabBarEmbedSegue" {
            if let tabc = segue.destinationViewController as? UITabBarController {
                contentContainerViewController = tabc
            }
        }
    }

}
