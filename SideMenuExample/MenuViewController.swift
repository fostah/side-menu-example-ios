//
//  MenuViewController.swift
//  SideMenuExample
//
//  Created by Michael Foster on 3/27/15.
//  Copyright (c) 2015 fostah.com. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate {
    func menuViewController(viewController: MenuViewController, didSelectMenuItemAtIndexPath indexPath: NSIndexPath)
}

class MenuViewController: UITableViewController {

    var delegate: MenuViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)                
        delegate?.menuViewController(self, didSelectMenuItemAtIndexPath: indexPath)
    }
}
